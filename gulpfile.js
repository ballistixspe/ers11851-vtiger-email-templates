var gulp = require('gulp');

gulp.task('compile', function () {
    'use strict';
    var twig = require('gulp-twig');
    return gulp.src('./templates/*.twig')
        .pipe(twig())
        .pipe(gulp.dest('./'));
});

gulp.task('default', ['compile']);
